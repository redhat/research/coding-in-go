# Coding in Go

## About the course

Go is a relatively new and popular programming language that supports source code compilation into native (machine) code, so the result is fast and memory-efficient applications comparable to the results produced by C, C++, D or Rust compilers. At the same time the Go language uses automatic memory management (GC - garbage collector), so-called goroutines and channels. At the same time Go is based on relatively minimalist syntax, which makes it quite significantly different from C or C++. Currently, Go is mainly used in containers and microservices ecosystems.

### Objectives

To inform course participants with all the important features of the Go programming language in such depth that they will be able to create and modify applications written in Go, while the resulting code will be effective and written in an idiomatic way.

### Outline

1. Elementary information about Go language
    - Origins of Go, Go versions
    - Go language use cases
    - Common features with other languages
    - Differences between Go and other languages
1. Go syntax and semantic
    - Keywords
    - Data types
    - Functions, functions visibility from other packages
    - Data structures
    - Interfaces
    - Program blocks
    - Variable visibility
    - Conditions
    - Program loops
    - Error handling
    - Operators
    - Arrays
    - Associative arrays (maps)
1. Concurrency and parallelism in Go
    - Goroutines
    - Channels - data structures used to communicate between goroutines
1. Practical examples
    - Working with sockets
    - HTTP server developed in Go
1. Go in real world
    - Testing
    - Benchmarking
    - Monitoring
1. Additional topics
    - Linters for Go
    - (Cross)compilation for different CPU architectures and for different OS

### Schedule

- 2023-09-19  First presentation
- 2023-10-03  Second presentation
- 2023-10-10  Third presentation
- 2023-10-17  Fourth presentation + deadline to choose PR/project/group project
- 2023-10-24  Fifth presentation
- 2023-10-31  Sixth presentation
- 2023-11-28  Projects presentation
- 2023-12-05  Projects deadline

### Lectors

- Ivan Nečas <inecas@redhat.com>
- Pavel Tišnovský <ptisnovs@redhat.com>

## Prerequisities

1. C language knowledge at middle level
1. Python language knowledge is optional, but help you much

## Learning materials

### Slides

Slides are available on https://github.com/RedHatOfficial/GoCourse

Please note that you need to have Go already installed in order to preview slides.

### Other materials

1. Tutorial: Get started with Go
https://go.dev/doc/tutorial/getting-started

1. Go doc
https://go.dev/doc/

1. A Tour of Go https://go.dev/tour/

1. The Why of Go (put many Go-related things into context)
www.youtube.com/watch?v=bmZNaUcwBt4&list=WL

1. GothamGo 2018 - Things in Go I Never Use by Mat Ryer
www.youtube.com/watch?v=5DVV36uqQ4E 

### Hacktoberfest 10

[Hacktoberfest](https://hacktoberfest.com) is here!

Interesting links:
- [Introduction to GitHub and Open-Source Projects](https://www.digitalocean.com/community/tutorial-series/an-introduction-to-open-source)
- [Github Go Hacktoberfest repositories](https://github.com/topics/hacktoberfest?l=go)
- [Hacktoberfest Go repositories](https://hacktoberfest-projects.vercel.app/repos/Go?p=2)
- [Up for Grabs Go](https://up-for-grabs.net/#/filters?tags=go)
- [First issue Go](https://firstissue.dev/language/go/)

## Credits etc.

### Project

You can choose:

- To successfully complete the course, the student will need to prove a significant time spent with the programming language. The options are:
  - pull-requests opened and accepted in a repository of a public project
  - a project (written in Go) you are interested in
- The amount of work spent on the project/pull-requests should be about 50 hours.
- The chosen way to finish the course needs to be acknowledged by the lectors.
- A larger project can be solved in a group of students. The size of the project needs to be proportional to the size of the team (3 members ~> 3x50 hours of work).

#### Pull requests

Although we would love to see you contribute to major projects such as [Prometheus](https://github.com/prometheus/prometheus), [Grafana](https://github.com/grafana/grafana), [NATS](https://github.com/nats-io/nats-server), [Traefik](https://github.com/traefik/traefik) or [Kubernetes](https://github.com/kubernetes/kubernetes) we realise it is no easy task. 
Therefore, pull request into reasonably sized projects will also be accepted. 
However, please consult your project choice with the lecturers. 

You can get started here:
- [Github trending projects](https://github.com/trending/go?since=monthly)
- [awesome-go repository](https://github.com/avelino/awesome-go)

#### Team project ideas

- Construction kit for creating stub for project for selected ecosystem (ask Pavel T.)
- Knowledge base - from log/stack trace to "how to fix info" (ask Pavel T.)
- Alerts deduplicator (ask Pavel T.)
- Source code transformation based on AST (ask Pavel T.)
- Cloned code detector (probably based on AST) (ask Pavel T.)
- Web scraper and data processor (ask Stanislav Z.)
- Messaging app (ask Stanislav Z.)
- Discord bot (ask Stanislav Z.)
- Jira clone (ask Stanislav Z.)
- SQL Server (ask Stanislav Z.)
